// tcprelay.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>

using namespace boost::asio;


class relay_connection : public std::enable_shared_from_this<relay_connection>
{
public:
	relay_connection(io_context& _ctx, ip::tcp::socket _socket) :
		ctx_(_ctx),
		client_(std::move(_socket)),
		server_(_ctx)
	{
	}

	void start()
	{
		ip::tcp::resolver r(ctx_);
		auto ep = r.resolve("127.0.0.1", "24727");

		boost::system::error_code ec;
		connect(server_, ep.begin(), ep.end(), ec);

		if (ec)
		{
			std::cerr << ec.message() << std::endl;
		}
		else
		{
			transfer(client_, server_, client_buffer_);
			transfer(server_, client_, server_buffer_);
		}
	}

protected:

	void transfer(ip::tcp::socket& _from, ip::tcp::socket& _to, std::array<uint8_t, 4096>& _buffer)
	{
		_from.async_read_some(buffer(_buffer), [&, me = shared_from_this()](const boost::system::error_code& _er, std::size_t _size) {
			
			if (_er)
				return;

			_to.async_send(buffer(_buffer, _size), [&, me = shared_from_this()](const boost::system::error_code& _er, std::size_t _size) {

				if (_er)
					return;

				transfer(_from, _to, _buffer);
			});
		});
	}


private:
	io_context& ctx_;
	ip::tcp::socket client_;
	ip::tcp::socket server_;
	
	std::array<uint8_t, 4096> client_buffer_;
	std::array<uint8_t, 4096> server_buffer_;
	
};

void do_accept(io_context& _ctx, ip::tcp::acceptor& _acceptor)
{
	static ip::tcp::socket s(_ctx);
	s = ip::tcp::socket(_ctx);

	_acceptor.async_accept(s, [&](boost::system::error_code ec) {
		if (ec)
		{
			std::cerr << ec.message() << std::endl;
			return;

		}

		auto ptr = std::make_shared<relay_connection>(_ctx, std::move(s));
		ptr->start();
		do_accept(_ctx, _acceptor);
	});
}

int main()
{
	io_context ctx;
	ip::tcp::acceptor acceptor(ctx);

	ip::tcp::endpoint ep(ip::tcp::v4(), 1235);
	acceptor.open(ep.protocol());
	acceptor.bind(ep);
	acceptor.listen();
	do_accept(ctx, acceptor);

	ctx.run();
}

